import React from "react"; // อย่าลืม import ทุกครั้งหล่ะตอนสร้าง component ใหม่ ไม่ใส่แม่งแจ้ง error ไว้หาคำตอบอีกที
import { connect } from "react-redux";

function ReduxCmp(props) {
  console.log(props);

  const greeting = (msg) => {
    return "greeting: " + msg;
  };

  return (
    <div className="">
      <h5>Redux new Component</h5>
      <hr></hr>
      <h5>
        {greeting("Hello")} i'm {props.name}
      </h5>
      <h1>counter : {props.counters}</h1>
      <div className="buttons">
        <button
          onClick={() => {
            props.dispatch({
              type: "INCREMENT",
              score: 5,
            });
          }}
          className="button is-warning is-light"
        >
          +5
        </button>
        <button
          onClick={() => {
            props.dispatch({
              type: "DECREMENT",
              score: 5,
            });
          }}
          className="button is-danger is-light"
        >
          -5
        </button>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => state;

const AppWithConnect = connect(mapStateToProps)(ReduxCmp);
export default AppWithConnect;
