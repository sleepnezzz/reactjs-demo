import React, { useState, useEffect, useCallback } from "react";
import { v4 as uuidv4 } from "uuid";

export default function Crud() {
  const initialState = { id: uuidv4(), firstname: "", lastname: "" };
  const [user, setUser] = useState(initialState);
  const [account, setAccount] = useState([
    {
      id: "a46666f9-847b-4629-aac6-fd257e8cc2f6",
      firstname: "A",
      lastname: "B",
    },
  ]);

  const [itemSelected, setItemSelected] = useState(null);

  function makeid(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  //   useEffect(() => {
  //     console.log("Do something after counter has changed", user);
  //   }, [user]);

  const onSubmit = () => {
    if (itemSelected) {
      const updateAccount = account.map((item) => {
        if (item.id === itemSelected) {
          return user;
        }
        return item;
      });
      setAccount(updateAccount);
      return console.log("update", user);
    }
    var u = {};
    if (user.firstname === "" && user.lastname === "") {
      console.log("generate user auto");
      u = {
        id: uuidv4(),
        firstname: makeid(10),
        lastname: makeid(5),
      };
      //   setUser(u);
      // setAccount([...account, user]); // not work
      setAccount([...account, u]);
    } else {
      setAccount([...account, { ...user, id: uuidv4() }]);
    }
    console.log("submit", u);
  };

  const onSelectedItem = (item) => {
    console.log("onSelectedItem", item);
    // set item select
    setItemSelected(item.id);
    setUser(item);
  };

  const onDelete = (item) => {
    // const idx = account.findIndex((i) => i.id === item.id);
    // const itemsArr = account;
    // itemsArr.splice(idx, 1);
    // setAccount([...itemsArr]);
    // console.log("onDelete on idx", idx, item);
    const accountList = account.filter((i) => i.id != item.id);
    setAccount(accountList);

    //   const updateAccount = account.map((item) => {
    //     if (item.id === itemSelected) {
    //       return user;
    //     }
    //     return item;
    //   });
    // or
    // const updateAccount = account.filter((item) => item.id != itemSelected);
    // setAccount(updateAccount);
  };

  const updateItem = () => {};

  const onClearItems = () => {
    if (window.confirm("Clear items?")) {
      setAccount([]);
    } else {
      alert("Cancle");
    }
  };

  return (
    <>
      {/* Form user account */}
      <h5>CRUD</h5>
      <hr></hr>
      <form
        className="form-inline"
        onSubmit={(e) => {
          onSubmit();
          e.preventDefault();
        }}
      >
        <label>name</label>
        <input
          type="text"
          className="form-inline"
          value={user.firstname}
          onChange={(e) => {
            setUser({ ...user, firstname: e.target.value });
          }}
        ></input>

        <label>lastname</label>
        <input
          type="text"
          value={user.lastname}
          className="form-inline"
          onChange={(e) => {
            setUser({ ...user, lastname: e.target.value });
          }}
        ></input>
        <input type="submit" value={itemSelected ? "update" : "submit"}></input>
        <input
          type="button"
          onClick={() => {
            setUser(initialState);
            setItemSelected(null);
          }}
          value="clear"
        ></input>
      </form>
      {/* Debugger */}
      {JSON.stringify(user)}
      {/* List users account */}
      <div className="my-3">
        <ul className="list-group">
          {account.map((item, i) => (
            <li
              className="list-group-item"
              style={{
                backgroundColor: item.id == itemSelected ? "#F0FFFF" : "",
              }}
              key={i}
            >
              <button
                onClick={() => {
                  onDelete(item);
                }}
              >
                X
              </button>
              <button
                onClick={() => {
                  onSelectedItem(item);
                }}
              >
                EDIT
              </button>
              {JSON.stringify(item)}
            </li>
          ))}
        </ul>
        {account.length ? (
          <button
            className="btn btn-sm btn-danger my-3"
            onClick={() => {
              onClearItems();
            }}
          >
            clear items
          </button>
        ) : null}
      </div>
    </>
  );
}
