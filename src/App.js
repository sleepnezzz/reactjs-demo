import React, { useState } from "react";
import "./App.css";
// components
import Crud from "./components/Crud";
import Redux from "./components/Redux";
import ReduxCmp from "./components/ReduxCmp";

function App() {
  return (
    <div className="">
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="#">
          TUTORIAL
        </a>
      </nav>
      <div className="container">
        <Crud></Crud>
      </div>
      <div className="container">
        <Redux></Redux>
      </div>
      <div className="container">
        <ReduxCmp name="new component in App component"></ReduxCmp>
      </div>
    </div>
  );
}

export default App;
