import React from "react";
import { connect } from "react-redux";

import { increment, decrement } from "../actions";

const Redux = ({ message, counter, dispatch }) => (
  <div className="my-3">
    <h5>Redux</h5>
    <hr></hr>
    {message}
    <div className="columns column is-12">
      <h1>Counter : {counter}</h1>
    </div>
    <div className="buttons">
      <button
        onClick={() => dispatch(increment(1))}
        className="button is-primary"
      >
        +1
      </button>
      <button onClick={() => dispatch(decrement(1))} className="button is-link">
        -1
      </button>
    </div>
  </div>
);

const mapStateToProps = function (state) {
  return {
    message: "This is message from mapStateToProps",
    counter: state.counters || 0,
  };
};

const AppWithConnect = connect(mapStateToProps)(Redux);
export default AppWithConnect;
