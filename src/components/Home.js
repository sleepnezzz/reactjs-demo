import React, { useState } from "react";
import axios from "axios";

export default function Home() {
  const initialState = { count: 55, keyword: "key" };
  const [keyword, setKeyword] = useState(initialState);

  const [account, setAccount] = useState([]);

  const search = (key) => {
    console.log(key);
    axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then((res) => {
        console.log(res);
        let { data } = res;
        setAccount(data);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const removeItem = (index) => {
    console.log("remove item index: ", index);
    let itemsArr = account;

    console.log(itemsArr);
    itemsArr.splice(index, 1);
    console.log(itemsArr);
    setAccount([...itemsArr]);
  };

  const editItem = (item) => {
    console.log(item);
  };

  return (
    <div>
      <table className="nav-bar">
        <tbody>
          <tr>
            <td>
              <img src={require("./assets/logo.svg")} width="50"></img>
            </td>
            <td>sleepy</td>
          </tr>
        </tbody>
      </table>
      <div>
        <p>#Debug {JSON.stringify(keyword)}</p>
        <p>#Debug {JSON.stringify(account)}</p>
      </div>
      <input
        type="text"
        placeholder="input"
        className="input-custom"
        onChange={(e) => setKeyword({ ...keyword, keyword: e.target.value })}
      ></input>
      <button
        onClick={() => {
          setKeyword({ ...keyword, count: keyword.count * 2 });
        }}
      >
        click
      </button>
      <button
        onClick={(e) => {
          e.preventDefault();
          setAccount([
            ...account,
            { key: keyword.count, index: account.length },
          ]);
        }}
      >
        set account list
      </button>
      <button
        onClick={(e) => {
          e.preventDefault();
          setKeyword(initialState);
          setAccount([]);
        }}
      >
        clear
      </button>
      <button
        onClick={() => {
          search(keyword);
        }}
      >
        call service
      </button>

      <ul>
        {account.map((item, index) => (
          <li key={index}>
            <button
              onClick={() => {
                editItem(item);
              }}
            >
              Edit
            </button>
            <button
              onClick={() => {
                removeItem(index);
              }}
            >
              {index} X
            </button>
            item {JSON.stringify(item)}
          </li>
        ))}
      </ul>
    </div>
  );
}
